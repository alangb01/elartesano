<?php

namespace App\Http\Controllers;

use App\Models\Sorteo;
use Illuminate\Http\Request;

class SorteosController extends Controller
{
    //
    public function index(){
        $sorteo=Sorteo::all()->first();
        $data=compact('sorteo');
        return view('sorteos.index',$data);
    }
}
