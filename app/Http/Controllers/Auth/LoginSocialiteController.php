<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginSocialiteController extends Controller
{
    //
    public function redirect($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider){
        $user_socialite=Socialite::driver($provider)->user();

        $user_auth=UserAuth::buscarAcceso($provider,$user_socialite->id)->first();

        if(!$user_auth instanceof UserAuth || !$user_auth->user instanceof User){
            return redirect()->route('login')->with(["error"=>"No se ha habilitado el acceso para ".$provider]);
        }

        Auth::login($user_auth->id_user);
        return redirect()->route('home');
    }
}
