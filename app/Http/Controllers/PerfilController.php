<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PerfilController extends Controller
{
    //
    public function show(){
        $user_profile=$this->userProfile();

        $data=compact('user_profile');

        return view("perfil.info",$data);
    }

    public function edit(){
        $user_profile=$this->userProfile();
        $data=compact('user_profile');
        return view("perfil.formulario",$data);
    }

    public function update(Request $request){
        $this->validate($request,[
            'nombres'=>'required',
            'apellidos'=>'required',
            'email'=>'required',
            'telefono'=>'required',
        ]);

        $user_profile=$this->userProfile();

        $user_profile->nombres=$request->input('nombres');
        $user_profile->apellido=$request->input('apellido');
        $user_profile->email=$request->input('email');
        $user_profile->telefono=$request->input('telefono');

        $user_profile->save();

        return redirect()->back()->with("status","Actualizado con exito");
    }

    private function userProfile(){
        $user=Auth::user();
        $user_profile=$user->profile;
        return $user_profile;
    }
}
