<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Pedido;
use App\Models\Tarifa;
use Illuminate\Http\Request;

class TarifasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:intranet');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $filtrar=$request->get('filtrar');
        $tarifas=Tarifa::filtrar($filtrar)->get();

        foreach($tarifas as $tarifa){
            $tarifa->url_editar=route('intranet.tarifas.editar',$tarifa->id);
        }

        $data=compact('tarifas','filtrar');
        return view('intranet.tarifas.listado',$data);    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tarifa=new Tarifa();
        $data=compact('tarifa');
        return view('intranet.tarifas.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'precio_unitario'=>'required',
//            'titulo'=>'required',
//            'descripcion'=>'required',
        ]);

        $tarifa=new Tarifa();
        $tarifa->precio_unitario=$request->input('precio_unitario');
        $tarifa->save();

        return redirect()->route('intranet.tarifas.listado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tarifa=Tarifa::findOrFail($id);
        $data=compact('tarifa');
        return view('intranet.tarifas.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'precio_unitario'=>'required',
        ]);

        $tarifa=Tarifa::findOrFail($id);
        $tarifa->precio_unitario=$request->input('precio_unitario');
        $tarifa->save();

        return redirect()->route('intranet.tarifas.listado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
