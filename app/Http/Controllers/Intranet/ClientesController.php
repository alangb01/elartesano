<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Pedido;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $filtrar=$request->get('filtrar');
        $clientes=Cliente::filtrar($filtrar)->paginate(10);
        foreach($clientes as $cliente){
            $cliente->url_editar=route('intranet.clientes.editar',$cliente->id);
            $cliente->url_pedido=route('intranet.pedidos.registrar');
        }

        $data=compact('clientes','filtrar');
        return view('intranet.clientes.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cliente=new Cliente();
        $data=compact('cliente');
        return view('intranet.clientes.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        $this->validate($request,[
//           'nombre'=>'required'
//        ]);

        $cliente=new Cliente();
        $cliente->alias=$request->get('alias');
        $cliente->nombres=$request->get('nombre');
        $cliente->apellidos=$request->get('apellido');
        $cliente->correo=$request->get('correo');
        $cliente->telefono=$request->get('telefono');

        $cliente->save();

        if($request->input('guardar')=="guardar_pedido"){
            $pedido=new Pedido();
            $pedido->id_cliente=$cliente->id;
            $pedido->save();
            return redirect()->route('intranet.pedidos.carrito',$pedido->id);
        }else{
            return redirect()->route('intranet.clientes.listado');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cliente=Cliente::findOrFail($id);
        $data=compact('cliente');
        return view('intranet.clientes.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cliente=Cliente::findOrFail($id);
        $cliente->alias=$request->get('alias');
        $cliente->nombres=$request->get('nombre');
        $cliente->apellidos=$request->get('apellido');
        $cliente->correo=$request->get('correo');
        $cliente->telefono=$request->get('telefono');
        $cliente->save();

        return redirect()->route('intranet.clientes.listado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
