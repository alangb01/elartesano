<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Despacho;
use App\Models\DetallePedido;
use App\Models\Direccion;
use App\Models\Pedido;
use App\Models\Tarifa;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PedidosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:intranet');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $filtrar=$request->get('filtrar');
        $pedidos=Pedido::filtrar($filtrar)->paginate(10);

        foreach($pedidos as $pedido){
            $pedido->url_editar=route('intranet.pedidos.carrito',$pedido->id);
            $pedido->url_confirmado=route('intranet.pedidos.resumen',$pedido->id);
        }

        $data=compact('pedidos','filtrar');
        return view('intranet.pedidos.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $id_cliente=$request->input('id_cliente');

        $pedido=new Pedido();
        $pedido->id_cliente=$id_cliente;

        $pedido->save();

        return redirect()->route('intranet.pedidos.listado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pedido=Pedido::findOrFail($id);
        $detallePedido=$pedido->detallePedido;
        $data=compact('pedido','detallePedido');
        return view('intranet.pedidos.completo',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function carrito($id)
    {
        //
        $pedido=Pedido::findOrFail($id);

        $tarifas=Tarifa::all();

        $detallePedido=$pedido->detallePedido;

        foreach($detallePedido as $detalle){
            $detalle->url_eliminar=route('intranet.pedidos.detalle.eliminar',[$detalle->id_pedido]);
            $detalle->url_actualizar=route('intranet.pedidos.detalle.actualizar',[$detalle->id_pedido]);
        }

        $pedido->url_cliente=route('intranet.pedidos.cliente',[$pedido->id]);

        $data=compact('pedido','detallePedido','tarifas');
        return view('intranet.pedidos.carrito',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCarrito(Request $request, $id)
    {
        //
        $id_detalle=$request->input('id_detalle');
        $accion=$request->input('accion');
        $cantidad=$request->input('cantidad');

        $detalle=DetallePedido::findOrFail($id_detalle);

        if($accion==0){
            $detalle->cantidad=$cantidad;
        }elseif($accion==-1 && $detalle->cantidad>1){
            $detalle->cantidad=$detalle->cantidad-1;
        }else{
            $detalle->cantidad=$detalle->cantidad+1;
        }

        $detalle->calcularSubtotal();
        $detalle->save();

        $pedido=$detalle->pedido;
        $pedido->calcularTotal();
        $pedido->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addProducto(Request  $request,$id_pedido){

        $id_tarifa=$request->input('tarifa');

        $tarifa=Tarifa::findOrFail($id_tarifa);

        $detalle=new DetallePedido();
        $detalle->id_pedido=$id_pedido;
        $detalle->producto=$tarifa->getDescripcion();
        $detalle->precio=$tarifa->precio_unitario;
        $detalle->cantidad=1;
        $detalle->calcularSubtotal();
        $detalle->save();

        $pedido=$detalle->pedido;
        $pedido->calcularTotal();
        $pedido->save();



        return redirect()->back();
    }

    public function removeProducto(Request $request){
        $id_detalle=$request->input('id_detalle');
        $detalle=DetallePedido::findOrFail($id_detalle);

        $detalle->delete();


        $pedido=$detalle->pedido;
        $pedido->calcularTotal();
        $pedido->save();

        return redirect()->back();
    }

    public function cliente(Request  $request, $id){
        $pedido=Pedido::findOrFail($id);
        $cliente=$pedido->cliente;

        if(!$cliente instanceof Cliente){
            $cliente=new Cliente();
            $cliente->nombres="nuevo cliente";
            $cliente->save();

            $pedido->id_cliente=$cliente->id;
            $pedido->save();
        }

        $pedido->url_programar=route('intranet.pedidos.programar',[$pedido->id]);

        $data=compact('pedido','cliente');
        return view('intranet.pedidos.cliente',$data);
    }

    public function updateCliente(Request $request,$id){
        $pedido=Pedido::findOrFail($id);
        $cliente=$pedido->cliente;

        $cliente->nombres=$request->get('nombre');
        $cliente->apellidos=$request->get('apellido');
        $cliente->correo=$request->get('correo');
        $cliente->telefono=$request->get('telefono');

        $cliente->save();

        return redirect()->back();
    }

    public function programar(Request  $request, $id){
        $pedido=Pedido::findOrFail($id);

        $select_fecha=$this->generarSelectfecha($pedido->entrega);
        $select_hora=$this->generarSelectHora($pedido->entrega);
        $select_entrega=$this->generarSelectTipoEntrega($pedido->tipo_entrega);

        $pedido->url_entrega=route('intranet.pedidos.entrega',[$pedido->id]);

        $data=compact('pedido','select_hora','select_fecha','select_entrega');
        return view('intranet.pedidos.programar',$data);
    }

    public function updateProgramar(Request $request,$id){
        $pedido=Pedido::findOrFail($id);
        $pedido->entrega=$request->input('fecha')." ".$request->input('hora').":00";
        $pedido->tipo_entrega=$request->input('tipo_entrega');
        $pedido->save();

        return redirect()->back();
    }

    public function entrega(Request $request,$id){
        $pedido=Pedido::findOrFail($id);
        if(isset($pedido->despacho)){
            $despacho=$pedido->despacho;
        }else{
            $despacho=new Despacho();
            $despacho->id_pedido=$pedido->id;
            $despacho->destinatario=$pedido->cliente->getNombreCompleto();
            $despacho->telefono=$pedido->cliente->telefono;
            $despacho->save();
        }

        $direcciones=$pedido->cliente->direcciones;

        $pedido->url_resumen=route('intranet.pedidos.resumen',[$pedido->id]);

        $data=compact('pedido','despacho','direcciones');
        return view('intranet.pedidos.entrega',$data);
    }

    public function storeEntrega(Request $request,$id){
        $pedido=Pedido::findOrFail($id);
        $despacho=$pedido->despacho;

        $despacho->destinatario=$request->input('destinatario');
        $despacho->telefono=$request->input('telefono');

        if($pedido->tipo_entrega==Pedido::TIPO_ENTREGA_DELIVERY){
            $despacho->direccion=$request->input('direccion');
            $despacho->referencia=$request->input('referencia');
            $despacho->latitud=$request->input('latitud');
            $despacho->longitud=$request->input('longitud');
            $despacho->zoom=$request->input('zoom');
            $despacho->estado=Despacho::ESTADO_PENDIENTE;
            $despacho->save();

            $this->guardarDireccionCliente($request,$pedido,$despacho);
        }elseif($pedido->tipo_entrega==Pedido::TIPO_ENTREGA_RECOJO){
            $despacho->direccion="Chinchaysuyo 728";
            $despacho->referencia="RECOJO EN LOCAL";
            $despacho->latitud="-6.7863519";
            $despacho->longitud="-79.8404806";
            $despacho->zoom="16";
            $despacho->estado=Despacho::ESTADO_PENDIENTE;
            $despacho->save();
        }

        return redirect()->back();
    }

    private function guardarDireccionCliente(Request  $request, Pedido $pedido, $despacho){
        $id_direccion=$request->input('id_direccion');

        $direccion=Direccion::find($id_direccion);
        if(!$direccion instanceof Direccion){
            $direccion=new Direccion();
            $direccion->id_cliente=$pedido->id_cliente;
        }

        $direccion->direccion=$despacho->direccion;
        $direccion->referencia=$despacho->referencia;
        $direccion->latitud=$despacho->latitud;
        $direccion->longitud=$despacho->longitud;
        $direccion->zoom=$despacho->zoom;
        $direccion->save();

    }

    private function generarSelectHora($entrega)
    {
        $timer=Carbon::now();
        $timer->setHour(18)->setMinute(30);

        $timer_limit=Carbon::now();
        $timer_limit->setHour(11)->setMinute(00);

        $select_hora=[];
        while($timer->format("hi")<=$timer_limit->format("hi")){
            $id=$timer->format('H:i');
            $valor=$timer->format('h:i a');

            $select_hora[]=(object)[
                'id'=>$id,
                'valor'=>$valor,
                'selected'=>$entrega instanceof Carbon &&  $entrega->format('Hi')==$timer->format("Hi")?'selected':''
            ];
            $timer->addMinutes(30);
        }

        return $select_hora;
    }

    private function generarSelectFecha($entrega)
    {
        $timer=Carbon::now();

        $timer_limit=Carbon::now();
        $timer_limit->addDays(14);

        $dias_semana=[
            Carbon::SUNDAY=>'domingo',
            Carbon::MONDAY=>'lunes',
            Carbon::TUESDAY=>'martes',
            Carbon::WEDNESDAY=>'miércoles',
            Carbon::THURSDAY=>'jueves',
            Carbon::FRIDAY=>'viernes',
            Carbon::SATURDAY=>'sábado',
        ];

        $select_fecha=[];
        while($timer->format("Ymd")<=$timer_limit->format("Ymd")){
            $id=$timer->format('Y-m-d');
            $valor=$timer->format('Y-m-d');
            $dia=$dias_semana[$timer->dayOfWeek];


            $diff_in_days=$timer->diffInDays(Carbon::now());
            if($diff_in_days==0){
                $valor.=" (hoy ".$dia.")";
            }elseif($diff_in_days==1){
                $valor.=" (mañana ".$dia.")";
            }else{
                $valor.=" (en $diff_in_days dias es ".$dia.")";
            }

            $select_fecha[]=(object)[
                'id'=>$id,
                'valor'=>$valor,
                'selected'=>$entrega instanceof Carbon && $entrega->format('Ymd')==$timer->format("Ymd")?'selected':''
            ];
            $timer->addDay();
        }

        return $select_fecha;
    }

    public function generarSelectTipoEntrega($tipo_entrega){
        $opciones=[
            (object)[
                'id'=>"R",
                'valor'=>"Recojo en local",
                'selected'=>$tipo_entrega==Pedido::TIPO_ENTREGA_RECOJO?'selected':''
            ],
            (object)[
                'id'=>"D",
                'valor'=>"Entrega a domicilio",
                'selected'=>$tipo_entrega==Pedido::TIPO_ENTREGA_DELIVERY?'selected':''
            ],
        ];

        return $opciones;


    }

    public function resumen(Request $request,$id){
        $pedido=Pedido::findOrFail($id);
        $detallePedido=$pedido->detallePedido;

        $data=compact('pedido','detallePedido');
        return view('intranet.pedidos.resumen',$data);
    }

    public function storeResumen(Request $request,$id){
        $pedido=Pedido::findOrFail($id);
        $pedido->estado=Pedido::ESTADO_CONFIRMADO;
        $pedido->save();


        return redirect()->back();
    }

}
