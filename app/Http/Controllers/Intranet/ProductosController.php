<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Pedido;
use App\Models\Producto;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:intranet');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $filtrar=$request->get('filtrar');
        $productos=Producto::filtrar($filtrar)->paginate(10);

        foreach($productos as $producto){
            $producto->url_editar=route('intranet.productos.editar',$producto->id);
        }

        $data=compact('productos','filtrar');
        return view('intranet.productos.listado',$data);    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $producto=new Producto();
        $data=compact('producto');
        return view('intranet.productos.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'titulo'=>'required',
            'descripcion'=>'required',
        ]);
        $producto=new Producto();
        $producto->titulo=$request->get('titulo');
        $producto->descripcion=$request->get('descripcion');

        $producto->save();

        return redirect()->route('intranet.productos.listado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $producto=Producto::findOrFail($id);
        $data=compact('producto');
        return view('intranet.productos.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'titulo'=>'required',
            'descripcion'=>'required',
        ]);

        $producto=Producto::findOrFail($id);
        $producto->titulo=$request->get('titulo');
        $producto->descripcion=$request->get('descripcion');
        $producto->save();

        return redirect()->route('intranet.productos.listado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
