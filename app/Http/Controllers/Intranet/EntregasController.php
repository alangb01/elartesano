<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Controllers\Controller;
use App\Models\Despacho;
use App\Models\Pedido;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EntregasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:intranet');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //
        $filtrar=$request->get('filtrar');
        $despachos=Despacho::conPedidoListo()
            ->orderBy('estado','desc')
            ->orderBy('p.entrega','asc')
            ->paginate(10);


//        foreach($pedidos as $pedido){
//            $pedido->url_editar=route('intranet.pedidos.carrito',$pedido->id);
//            $pedido->url_confirmado=route('intranet.pedidos.visualizar',$pedido->id);
//        }

        $data=compact('despachos','filtrar');
        return view('intranet.entregas.listado',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $id_despacho=$request->input('id_despacho');
        $despacho=Despacho::findOrFail($id_despacho);
        $despacho->estado=Despacho::ESTADO_ENTREGADO;
        $despacho->entregado=Carbon::now();
        $despacho->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
