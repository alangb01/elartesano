<?php

namespace App\Http\Controllers;


use App\Models\Sorteo;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hoy=Carbon::now();
        $sorteo=Sorteo::where('programado','>',$hoy)->first();
        $sorteo->url_detalle=route('sorteo.index');
        $data=compact('sorteo');
        return view('home',$data);
    }


}
