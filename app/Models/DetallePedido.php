<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetallePedido extends Model
{
    use HasFactory;

    protected $table="detalle_pedidos";
    public $timestamps=false;

    public function pedido(){
        return $this->belongsTo(Pedido::class,'id_pedido','id');
    }

    public function calcularSubtotal(){
        $this->subtotal=$this->cantidad*$this->precio;
    }
}
