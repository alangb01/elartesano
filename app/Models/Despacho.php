<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Despacho extends Model
{
    use HasFactory;

    const ESTADO_ENTREGADO = "E";
    const ESTADO_PENDIENTE = "P";

    public $dates=['entregado'];

    public function scopeConPedidoListo($query){
        $query->select("despachos.*");
        $query->join("pedidos as p","p.id","=","id_pedido");
        $query->where('p.estado_produccion',Pedido::ESTADO_PRODUCCION_LISTO);
        return $query;
    }

    public function pedido(){
        return $this->belongsTo(Pedido::class,'id_pedido','id');
    }

    public function getUrlUbicacion(){
        //return "https://maps.google.com/?ll=".$this->latitud.",".$this->longitud."&z=".$this->zoom."&t=k";
        return "https://maps.google.com/?q=".$this->latitud.",".$this->longitud."";
    }
}
