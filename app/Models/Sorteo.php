<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sorteo extends Model
{
    use HasFactory;
    public $dates=['programado'];

    public function participantes(){
        return $this->hasMany(Participante::class,'id_sorteo','id');
    }

    public function ganador(){
        return $this->hasOne(Participante::class,'ganador','id');
    }
}
