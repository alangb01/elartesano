<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    public function scopeFiltrar($query,$filtro){
        if(isset($filtro)){
            $query->where('alias','like','%'.$filtro.'%');
            $query->where('nombres','like','%'.$filtro.'%');
            $query->where('apellidos','like','%'.$filtro.'%');
            $query->where('correo','like','%'.$filtro.'%');
            $query->where('telefono','like','%'.$filtro.'%');
        }

        return $query;
    }

    public function getNombreCompleto(){
        return $this->nombres." ".$this->apellidos;
    }

    public function direcciones(){
        return $this->hasMany(Direccion::class,'id_cliente','id');
    }
}
