<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAuth extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class,'id_user','id');
    }

    public function scopeBuscarAcceso($query,$provider,$id){
        $query->where('provider',$provider)->where('identifier',$id);
        return $query;
    }
}
