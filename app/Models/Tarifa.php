<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarifa extends Model
{
    use HasFactory;

    public function producto(){
        return $this->belongsTo(Producto::class,'id_producto','id');
    }

    public function dimension(){
        return $this->belongsTo(Dimension::class,'id_dimension','id');
    }

    public function getDescripcion(){
        return $this->producto->titulo." - ".$this->dimension->nombre;
    }

    public function scopeFiltrar($query,$filtro){
        $query->select('tarifas.*');
        $query->join('productos as p','p.id','=','tarifas.id_producto');
        $query->join('dimensiones as d','d.id','=','tarifas.id_dimension');

        if(isset($filtro)){
            $query->where('p.titulo','like','%'.$filtro.'%');
            $query->where('p.descripcion','like','%'.$filtro.'%');
            $query->where('d.nombre','like','%'.$filtro.'%');
        }

        return $query;
    }
}
