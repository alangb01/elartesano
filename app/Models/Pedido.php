<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;



    protected $dates=['entrega'];

    const ESTADO_SIN_CONFIRMAR = "S";
    const ESTADO_CONFIRMADO = "C";

    const ESTADO_PRODUCCION_PENDIENTE = "P";
    const ESTADO_PRODUCCION_LISTO = "L";

    const TIPO_ENTREGA_DELIVERY="D";
    const TIPO_ENTREGA_RECOJO="R";

    public function scopeFiltrar($query,$filtro){
        if(isset($filtro)){
            $query->where('cliente','like','%'.$filtro.'%');
        }
        return $query;
    }

    public function scopeConEstado($query,$estado){
        $query->where('estado',$estado);
        return $query;
    }

    public function detallePedido(){
        return $this->hasMany(DetallePedido::class,'id_pedido','id');
    }

    public function despacho(){
        return $this->hasOne(Despacho::class,'id_pedido','id');
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class,'id_cliente','id');
    }

    public function calcularTotal(){
        $this->total=$this->detallePedido->sum("subtotal");
    }

    public function getCodigoNumerado(){
        return sprintf("%'.04d\n", $this->id);
    }

    public function getTipoEntrega(){
        if($this->tipo_entrega==self::TIPO_ENTREGA_DELIVERY){
            return "Delivery";
        }elseif($this->tipo_entrega==self::TIPO_ENTREGA_RECOJO){
            return "Recojo en local";
        }
    }
}
