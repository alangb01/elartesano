<?php

use App\Http\Controllers\Intranet\Auth\LoginController;
use App\Http\Controllers\Intranet\Auth\ConfirmPasswordController;
use App\Http\Controllers\Intranet\Auth\ForgotPasswordController;
use App\Http\Controllers\Intranet\Auth\RegisterController;
use App\Http\Controllers\Intranet\Auth\ResetPasswordController;
use App\Http\Controllers\Intranet\Auth\VerificationController;
use App\Http\Controllers\Intranet\HomeController;
use App\Http\Controllers\Intranet\ClientesController;
use App\Http\Controllers\Intranet\EntregasController;
use App\Http\Controllers\Intranet\PedidosController;
use App\Http\Controllers\Intranet\ProduccionController;
use App\Http\Controllers\Intranet\ProductosController;
use App\Http\Controllers\Intranet\TarifasController;

use Illuminate\Support\Facades\Route;






