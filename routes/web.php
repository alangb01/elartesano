<?php

use App\Http\Controllers\Auth\LoginSocialiteController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PerfilController;
use App\Http\Controllers\SorteosController;
use App\Http\Controllers\WebController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[WebController::class,'home']);
Route::get('/politica',[WebController::class,'politica']);

//Route::get('login/{provider}',[LoginSocialiteController::class,'redirect']);
//Route::get('login/{provider}/callback',[LoginSocialiteController::class,'callback']);
//
//Route::get("politica",[HomeController::class,'politica']);
//
//Auth::routes(['register' => true,'reset'=>true,'verify'=>true]);
//
//Route::middleware("verified")->group(function(){
//    Route::get('/dashboard', [HomeController::class, 'index'])->name('home');
//
//    //PERFIL
//    Route::prefix("perfil")->group(function(){
//        Route::get('editar', [PerfilController::class, 'show'])->name('sorteo.index');
//        Route::put('editar', [PerfilController::class, 'update']);
//
////        Route::get('oauth', [OAuthController::class, 'edit'])->name('login.oauth');
////        Route::put('oauth', [OAuthController::class, 'update']);
//    });
//
//    //SORTEOS
//    Route::prefix("sorteos")->group(function(){
//        Route::get('/', [SorteosController::class, 'index'])->name('sorteo.index');
//        Route::get('registrar', [SorteosController::class, 'create'])->name('sorteo.registrar');
//        Route::post('registrar', [SorteosController::class, 'store']);
//    });
//});

//Login
Route::get('login',[LoginController::class,'showLoginForm'])->name('login');
Route::post('login',[LoginController::class,'login']);

//Logout
Route::post('logout',[LoginController::class,'logout'])->name('logout');

//Register
Route::get('register',[RegisterController::class,'showRegistrationForm'])->name('register');
Route::post('register',[RegisterController::class,'register']);

//Reset password
Route::get('password/reset',[ForgotPasswordController::class,'showLinkRequestForm'])->name('password.request');
Route::post('password/email',[ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}',[ResetPasswordController::class,'showResetForm'])->name('password.reset');
Route::post('password/reset',[ResetPasswordController::class,'reset'])->name('password.update');

//Confirm
Route::get('password/confirm', [ConfirmPasswordController::class,'showConfirmForm'])->name('password.confirm');
Route::post('password/confirm',  [ConfirmPasswordController::class,'confirm']);

//Email verification
Route::get('email/verify', [VerificationController::class,'show'])->name('verification.notice');
Route::get('email/verify/{id}/{hash}', [VerificationController::class,'verify'])->name('verification.verify');
Route::post('email/resend', [VerificationController::class,'resend'])->name('verification.resend');

Route::prefix("intranet")->name("intranet.")->group(function() {

    Route::get('/', function () {
        return redirect()->route('intranet.home');
    });



    //DASHBOARD
    Route::get('/dashboard', [HomeController::class, 'index'])->name('home');
    //CLIENTES
    Route::name("clientes.")->group(function(){
        Route::get('clientes', [ClientesController::class,"index"])->name("listado");

        Route::get('clientes/registrar', [ClientesController::class,"create"])->name('crear');
        Route::post('clientes/registrar', [ClientesController::class,"store"]);
        Route::get('clientes/{id_cliente}/editar', [ClientesController::class,"edit"])->name('editar');
        Route::put('clientes/{id_cliente}/editar', [ClientesController::class,"update"]);
//    Route::delete('clientes/{id_cliente}/eliminar', 'ClientesController@edit');
    });

    //PEDIDOS
    Route::name("pedidos.")->group(function(){
        Route::get('pedidos', [PedidosController::class,"index"])->name("listado");

        //PEDIDO - PASO 0 - Seleccionar cliente
//        Route::get('pedidos/registrar', [PedidosController::class,"create"])->name('crear');
        Route::post('pedidos/registrar', [PedidosController::class,"store"])->name('registrar');

        //PEDIDO - PASO 1 - Agregar productos
        Route::get('pedidos/{id_pedido}/carrito',  [PedidosController::class,"carrito"])->name('carrito');
        Route::put('pedidos/{id_pedido}/carrito',  [PedidosController::class,"updateCarrito"])->name('detalle.actualizar');
        Route::post('pedidos/{id_pedido}/carrito',  [PedidosController::class,"addProducto"]);
        Route::delete('pedidos/{id_pedido}/carrito',  [PedidosController::class,"removeProducto"])->name('detalle.eliminar');

        //PEDIDO - PASO 2 - Actualizar datos de cliente
        Route::get('pedidos/{id_pedido}/cliente',  [PedidosController::class,"cliente"])->name('cliente');
        Route::put('pedidos/{id_pedido}/cliente',  [PedidosController::class,"updateCliente"]);

        //PEDIDO - PASO 3 - Programar entrega
        Route::get('pedidos/{id_pedido}/programar',  [PedidosController::class,"programar"])->name('programar');
        Route::put('pedidos/{id_pedido}/programar',  [PedidosController::class,"updateProgramar"]);

        //PEDIDO - PASO 4 - Seleccionar ubicacion
        Route::get('pedidos/{id_pedido}/entrega', [PedidosController::class,"entrega"])->name('entrega');
        Route::put('pedidos/{id_pedido}/entrega', [PedidosController::class,"storeEntrega"]);

        //PEDIDO - PASO 5 - Confirmar
        Route::get('pedidos/{id_pedido}/resumen', [PedidosController::class,"resumen"])->name('resumen');
        Route::post('pedidos/{id_pedido}/resumen', [PedidosController::class,"storeResumen"]);

//    Route::delete('clientes/{id_cliente}/eliminar', 'ClientesController@edit');

        //PEDIDO - PASO 4 - Confirmar pedido

        //PEDIDO - VISUALIZAR CONFIRMADO
        Route::get('pedidos/{id_pedillo}/visualizar',[PedidosController::class,"show"])->name('visualizar');
    });

    //PRODUCCION
    Route::name("produccion.")->group(function() {
        Route::get('produccion', [ProduccionController::class, "index"])->name("listado");
        Route::post('produccion', [ProduccionController::class, "store"])->name("actualizar");
    });

    //DESPACHOS
    Route::name("entregas.")->group(function() {
        Route::get('entregas', [EntregasController::class, "index"])->name("listado");
        Route::post('entregas', [EntregasController::class, "store"])->name("actualizar");
    });

    //PRODUCTOS
    Route::name("productos.")->group(function(){
        Route::get('productos', [ProductosController::class,"index"])->name("listado");

        Route::get('productos/registrar', [ProductosController::class,"create"])->name('crear');
        Route::post('productos/registrar', [ProductosController::class,"store"]);
        Route::get('productos/{id_producto}/editar',[ProductosController::class,"edit"])->name('editar');
        Route::put('productos/{id_producto}/editar', [ProductosController::class,"update"]);
//    Route::delete('clientes/{id_cliente}/eliminar', 'ClientesController@edit');
    });

    Route::name("tarifas.")->group(function(){
        Route::get('tarifas', [TarifasController::class,"index"])->name("listado");

        Route::get('tarifas/registrar', [TarifasController::class,"create"])->name('crear');
        Route::post('tarifas/registrar', [TarifasController::class,"store"]);
        Route::get('tarifas/{id_tarifa}/editar',[TarifasController::class,"edit"])->name('editar');
        Route::put('tarifas/{id_tarifa}/editar', [TarifasController::class,"update"]);
//    Route::delete('clientes/{id_cliente}/eliminar', 'ClientesController@edit');
    });

});
