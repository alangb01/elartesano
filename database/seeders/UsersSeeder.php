<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('users')->truncate();
        User::create([
            'name'=>'Alan',
            'email'=>'alangb01@gmail.com',
            'password'=>bcrypt('030496eagb')
        ]);
    }
}
