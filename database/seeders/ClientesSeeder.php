<?php

namespace Database\Seeders;

use App\Models\Cliente;
use Illuminate\Database\Seeder;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->registrarClientes();
    }

    private function registrarClientes(){
        \DB::table('clientes')->truncate();

        $clientes=[
            (object)['alias'=>"fiestas",'nombre'=>'Jose Martin','apellido'=>'Fiestas Guarniz','correo'=>'','telefono'=>''],
            (object)['alias'=>"Jorge",'nombre'=>'Jorge','apellido'=>'Hernandez','correo'=>'','telefono'=>''],
            (object)['alias'=>"Merly",'nombre'=>'Merly','apellido'=>'Uchofen','correo'=>'','telefono'=>''],
//            (object)['nombre'=>'Merly','apellido'=>'Uchofen','correo'=>'','telefono'=>''],
        ];

        foreach($clientes as $cliente){
            $this->registrarCliente($cliente->alias,$cliente->nombre,$cliente->apellido,$cliente->correo,$cliente->telefono);
        }
    }

    private function registrarCliente($alias,$nombre,$apellido="",$correo="",$telefono=""){
        $cliente=new Cliente();
        $cliente->alias=$alias;
        $cliente->nombres=$nombre;
        $cliente->nombres=$nombre;
        $cliente->apellidos=$apellido;
        $cliente->correo=$correo;
        $cliente->telefono=$telefono;
        $cliente->save();
    }
}
