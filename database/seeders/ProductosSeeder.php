<?php

namespace Database\Seeders;

use App\Models\Dimension;
use App\Models\Producto;
use App\Models\Tarifa;
use Illuminate\Database\Seeder;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->registrarProductos();
        $this->registrarDimensiones();

        $this->registrarTarifas();
    }

    private function registrarProductos(){
        \DB::table("productos")->truncate();

        $productos=[
            'Pizza Hawaiana'=>'Jamón y Chorizo',
            'Pizza de Chorizo'=>'Chorizo',
            'Pizza de Hot Dog Ahumado'=>'Hot Dog Ahumado',
            'Pizza de Atun'=>'Atún y cebolla',
            'Pizza 4 estaciones'=>'Jamón, Chorizo, Salame y Champiñones',
            'Pizza de Jamón'=>'Jamón',
            'Pizza de Jamón y Chorizo'=>'Jamón y Chorizo'
        ];

        foreach($productos as $titulo=>$descripcion){
            $producto=new Producto();
            $producto->titulo=$titulo;
            $producto->descripcion=$descripcion;
            $producto->save();
        }
    }

    private function registrarDimensiones(){
        \DB::table("dimensiones")->truncate();
        $dimensiones=[
            'Media'=>'Mitad de familiar',
            'Familiar'=>'32 cm de diametro',
            'Extra familiar'=>'36 cm de diametro',
        ];

        foreach($dimensiones as $nombre=>$descripcion){
            $dimension=new Dimension();
            $dimension->nombre=$nombre;
            $dimension->descripcion=$descripcion;
            $dimension->save();
        }
    }

    private function registrarTarifas(){

        \DB::table("tarifas")->truncate();

        $productos=Producto::all();
        $dimensiones=Dimension::all();

        foreach($productos as $producto){
            foreach($dimensiones as $dimension){
                $tarifa=new Tarifa();
                $tarifa->id_producto=$producto->id;
                $tarifa->id_dimension=$dimension->id;
                $tarifa->costo_unitario=0.0;
                $tarifa->precio_unitario=0.0;
                $tarifa->save();
            }

        }
    }
}
