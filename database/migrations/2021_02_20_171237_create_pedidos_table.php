<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user')->default(0);
            $table->integer('id_cliente')->default(0);
            $table->double('total')->default(0,0);
            $table->double('igv')->default(0,0);
            $table->string('tipo_entrega')->default('R'); //Recojo o Delivery
            $table->dateTime('entrega')->nullable();
            $table->string('tipo_pago')->default('E'); //(T)RANSFERENCIA (E)FECTIVO,
            $table->string('estado_produccion')->default('P'); //PENDIENTE LISTO
            $table->string('estado')->default('S'); //SIN CONFIRMAR - CONFIRMADO
            $table->timestamps();
        });

        Schema::create('detalle_pedidos', function (Blueprint $table) {
            $table->id();
            $table->integer('id_pedido')->default(0);
            $table->string('producto');
            $table->integer('cantidad');
            $table->double('precio');
            $table->double('subtotal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_pedidos');
        Schema::dropIfExists('pedidos');
    }
}
