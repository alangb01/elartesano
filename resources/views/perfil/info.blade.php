@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <a href="{{ route('intranet.clientes.listado') }}" class="float-right btn btn-secondary mb-2">Volver</a>
            <form method="post">
                @csrf
                @if($cliente->id>0)
                    @method('PUT')
                @endif

                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="nombresHelp" placeholder="Ingresa un nombre" value="{{ old('nombre',$cliente->nombres) }}">
                    {{--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                </div>
                <div class="form-group">
                    <label for="apellidos">Apellido</label>
                    <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Ingresa un apellido" value="{{ old('apellido',$cliente->apellidos) }}">
                </div>
                <div class="form-group">
                    <label for="correo">Correo</label>
                    <input type="email" class="form-control" id="correo" name="correo" aria-describedby="emailHelp" placeholder="Ingrese un correo" value="{{ old('correo',$cliente->correo) }}">
                    {{--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                </div>

                <div class="form-group">
                    <label for="telefono">Teléfono </label>
                    <input type="telefono" class="form-control" id="telefono" name="telefono" aria-describedby="emailHelp" placeholder="Ingrese un teléfono" value="{{ old('telefono',$cliente->telefono) }}">
                    {{--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                </div>

                <button type="submit" name="guardar" class="btn btn-primary" value="guardar">Guardar</button>
                <button type="submit" name="guardar" class="btn btn-secondary" value="guardar_pedido">Guardar y generar pedido</button>
            </form>

        </div>
    </div>
@endsection
