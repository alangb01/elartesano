@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="text-center">
                @isset($sorteo)
                    <a href="{{ $sorteo->url_detalle }}" class="btn btn-success">{{ $sorteo->titulo }}</a>
                @else
                    No hay sorteos disponibles
                @endisset
            </div>
        </div>
    </div>
</div>
@endsection
