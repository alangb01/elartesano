@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <h3 class="float-left">Registrar producto</h3>
            <a href="{{ route('intranet.productos.listado') }}" class="float-right btn btn-secondary mb-2">Volver</a>
            <div class="clearfix"></div>
            <form method="post">
                @csrf
                @if($producto->id>0)
                    @method('PUT')
                @endif

                <div class="form-group">
                    <label for="titulo">Nombre</label>
                    <input type="text" class="form-control" id="titulo" name="titulo" aria-describedby="titulosHelp" placeholder="Ingresa un titulo" value="{{ old('titulo',$producto->titulo) }}">
{{--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripcion</label>
                    <textarea class="form-control" rows="5" id="descripcion" name="descripcion" placeholder="Ingresa una descripcion">{{ old('descripcion',$producto->descripcion) }}</textarea>
                </div>

                <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
            </form>

        </div>
    </div>
@endsection
