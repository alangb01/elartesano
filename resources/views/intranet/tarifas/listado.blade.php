@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <form class="form-inline float-left">
                <div class="form-group mb-2">
                    <h4>Listado de tarifas</h4>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="filtrar" class="sr-only">Filtrar</label>
                    <input type="text" class="form-control" id="filtrar" name="filtrar" placeholder="Ingrese una palabra" value="{{ old('filtrar') }}">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Filtrar</button>
            </form>
            <a href="{{ route('intranet.tarifas.crear') }}" class="float-right btn btn-success mb-2">Registrar</a>
        </div>
        <table class="table table-bordered table-sm">
            <thead class="thead-light">
            <tr>
{{--                <th scope="col">#</th>--}}
                <th scope="col">Producto</th>
                <th scope="col">Dimension</th>
                <th scope="col">Precio</th>

                <th scope="col">Opciones</th>
            </tr>
            </thead>
            <tbody>
            @isset($tarifas)
                @forelse($tarifas as $tarifa)
                    <tr>
{{--                        <th></th>--}}
                        <td>{{$tarifa->producto->titulo}}</td>
                        <td>{{$tarifa->dimension->nombre}}</td>
                        <td>{{$tarifa->precio_unitario}}</td>
                        <td>
                            <a href="{{ $tarifa->url_editar }}" class="btn btn-primary btn-sm">Editar</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5"> No hay resultados</td>
                    </tr>
                @endforelse
            @else
                <tr>
                    <td  colspan="5"> No se han definido datos</td>
                </tr>
            @endisset
            </tbody>
        </table>
    </div>
@endsection
