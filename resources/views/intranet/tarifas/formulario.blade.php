@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <h3 class="float-left">Registrar tarifa</h3>
            <a href="{{ route('intranet.tarifas.listado') }}" class="float-right btn btn-secondary mb-2">Volver</a>
            <div class="clearfix"></div>
            <form method="post">
                @csrf
                @if($tarifa->id>0)
                    @method('PUT')
                @endif

                <div class="form-group">
                    <label for="titulo">Producto</label>
                    <input type="text" class="form-control" id="titulo" name="titulo" aria-describedby="titulosHelp" placeholder="Ingresa un titulo" value="{{ old('titulo',$tarifa->producto->titulo) }}" readonly>
{{--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                </div>
                <div class="form-group">
                    <label for="descripcion">Dimension</label>
                    <input type="text" class="form-control" id="dimension" name="dimension" aria-describedby="dimensionHelp" placeholder="Ingresa un dimension" value="{{ old('dimension',$tarifa->dimension->nombre) }}" readonly>

                </div>
                <div class="form-group">
                    <label for="titulo">Precio S/</label>
                    <input type="text" class="form-control" id="precio_unitario" name="precio_unitario" aria-describedby="precioHelp" placeholder="Ingresa un precio" value="{{ old('precio_unitario',$tarifa->precio_unitario) }}" >
                    {{--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                </div>
                <button type="submit" name="guardar" class="btn btn-primary">Guardar</button>
            </form>

        </div>
    </div>
@endsection
