@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <form class="form-inline float-left">
                <div class="form-group mb-2">
                    <h4>Listado de entregas</h4>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="filtrar" class="sr-only">Filtrar</label>
                    <input type="text" class="form-control" id="filtrar" name="filtrar" placeholder="Ingrese una palabra" value="{{ old('filtrar') }}">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Filtrar</button>
            </form>
            {{--            <a href="{{ route('intranet.pedidos.crear') }}" class="float-right btn btn-success mb-2">Registrar</a>--}}
        </div>
        <div class="clearfix"></div>
        @isset($despachos)
            @if($despachos->count())
                <div class="card-columns">

                    @foreach($despachos as $despacho)
                        @php
if($despacho->estado==\App\Models\Despacho::ESTADO_ENTREGADO){
    $clase_container="border-success ";
    $clase_header="bg-success text-white";
}else{
    $clase_container="border-primary";
    $clase_header="bg-primary text-white";
}

                        @endphp
                        <div class="card  {{ $clase_container }} mb-3" >
                            <div class="card-header  {{ $clase_header }} "> Pedido {{ $despacho->pedido->getCodigoNumerado() }} </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">Entregar a </div>
                                    <div class="col-6 font-weight-bold"> {{ $despacho->destinatario }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Teléfono</div>
                                    <div class="col-6  font-weight-bold"> {{ $despacho->telefono }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-6">Direccion</div>
                                    <div class="col-6  font-weight-bold">
                                        {{ $despacho->direccion }}
                                        {{ $despacho->referencia }}
                                        <a href="{{ $despacho->getUrlUbicacion() }}" class="btn btn-outline-info" target="_blank" class=""> <i class="fas fa-map-marker-alt"></i> Ubicación</a>
                                    </div>
                                </div>
                            </div>
{{--                            <div class="card-body text-primary">--}}
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($despacho->pedido->detallePedido as $detalle)
                                        <tr>
                                            <td>{{ $detalle->producto }}</td>
                                            <td class="text-center">{{ $detalle->cantidad }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

{{--                            </div>--}}
                            <div class="card-footer">
                                <div class="row">

                                    <div class="col-6">
                                        Entrega: {{ $despacho->pedido->entrega->format("h:i a") }}
                                    </div>
                                    <div class="col-6">
                                        @if($despacho->estado==\App\Models\Despacho::ESTADO_PENDIENTE)
                                            <form action="" method="post" class="form-inline ">
                                                @csrf
                                                <input type="hidden" name="id_despacho" value="{{ $despacho->id }}">
                                                <button type="submit" class="btn btn-success btn-sm">Marcar como entregado</button>
                                            </form>
                                        @elseif($despacho->estado==\App\Models\Despacho::ESTADO_ENTREGADO)
                                            Entregado: {{ $despacho->entregado->format("h:i a") }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            @else
                No hay pedidos confirmados
            @endif
        @else
            No se han detectado datos
        @endif


    </div>
@endsection
