@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <form class="form-inline float-left">
                <div class="form-group mb-2">
                    <h4>Listado de pedidos</h4>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="filtrar" class="sr-only">Filtrar</label>
                    <input type="text" class="form-control" id="filtrar" name="filtrar" placeholder="Ingrese una palabra" value="{{ old('filtrar') }}">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Filtrar</button>
            </form>
{{--            <form action="{{ route('intranet.pedidos.registrar') }}" method="post">--}}
{{--                @csrf--}}
{{--                <input type="hidden" name="id_cliente" value="0">--}}
{{--                <button type="submit" class="float-right btn btn-success mb-2">Registrar pedido</button>--}}
{{--            </form>--}}
        </div>
        <table class="table table-bordered table-sm">
            <thead class="thead-light">
            <tr>
{{--                <th scope="col">#</th>--}}
                <th scope="col">Cliente/Destinatario</th>
                <th scope="col">Total</th>
                <th scope="col">Entrega</th>
                <th scope="col">Registrado</th>
                <th scope="col">Estado</th>
                <th scope="col">Opciones</th>
            </tr>
            </thead>
            <tbody>
            @isset($pedidos)
                @forelse($pedidos as $pedido)
                    <tr>
{{--                        <th></th>--}}
                        <td>
                            @isset($pedido->cliente)
                                {{$pedido->cliente->getNombreCompleto()}}
                            @else
                                No existe el cliente
                            @endif
                            @isset($pedido->despacho)
                                // <strong>{{$pedido->despacho->destinatario}}</strong>
                            @endisset
                        </td>
                        <td>{{$pedido->total}}</td>
                        <td>
                            @isset($pedido->entrega)
                                {{$pedido->entrega->format("Y-m-d h:i a")}}
                                @else
                                No se ha programado
                            @endif
                        </td>
                        <td>
                            {{$pedido->created_at->format("Y-m-d h:i a")}}
                        </td>
                        <td>
                            @if($pedido->tipo_entrega==\App\Models\Pedido::TIPO_ENTREGA_RECOJO)
                                <i class="fas fa-home"></i>  Recojo en local
                            @elseif($pedido->tipo_entrega==\App\Models\Pedido::TIPO_ENTREGA_DELIVERY)
                                <i class="fas fa-motorcycle"></i> Delivery
                            @endif
                        </td>
                        <td>
                            @if($pedido->estado==\App\Models\Pedido::ESTADO_SIN_CONFIRMAR)
                                <a href="{{ $pedido->url_editar }}" class="btn btn-warning btn-sm">Editar</a>
                            @elseif($pedido->estado==\App\Models\Pedido::ESTADO_CONFIRMADO)
                                <a href="{{ $pedido->url_confirmado }}" class="btn btn-success btn-sm">Visualizar</a>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5"> No hay resultados</td>
                    </tr>
                @endforelse
            @else
                <tr>
                    <td  colspan="5"> No se han definido datos</td>
                </tr>
            @endisset
            </tbody>
        </table>
    </div>
@endsection
