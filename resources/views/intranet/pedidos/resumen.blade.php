@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <a href="{{ route('intranet.pedidos.listado') }}" class="float-right btn btn-secondary mb-2">Volver</a>
            <div class="clearfix"></div>
            <div class="card border-secondary mb-2" >
                <div class="card-header text-white bg-secondary">
                    Datos de pedido - <strong>{{ $pedido->getCodigoNumerado() }}</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="fecha">Cliente</label>
                            <input type="text" class="form-control" value="{{ $pedido->cliente->getNombreCompleto() }}" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fecha">Registrado</label>
                            <input type="text" class="form-control" value="{{ $pedido->created_at->format("Y-m-d h:i a") }}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-secondary mb-2" >
                <div class="card-header text-white bg-secondary">
                    Detalle de pedido
                </div>
                <div class="card-body">
                    <table class="table table-bordered border-secondary table-sm">
                        <thead class="thead-light">
                        <tr>
                            {{--                <th scope="col">#</th>--}}
                            <th scope="col">Producto</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        @isset($detallePedido)
                            @forelse($detallePedido as $detalle)
                                <tr>
                                    {{--                        <th></th>--}}
                                    <td>{{$detalle->producto}}</td>
                                    <td>{{$detalle->cantidad}}</td>
                                    <td>{{$detalle->precio}}</td>
                                    <td>{{$detalle->subtotal}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5"> No hay resultados</td>
                                </tr>
                            @endforelse
                            <tr>
                                <th colspan="3" class="text-right">Total</th>
                                <th>{{ $pedido->total }}</th>
                            </tr>
                        @else
                            <tr>
                                <td  colspan="5"> No se han definido datos</td>
                            </tr>
                        @endisset
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card border-secondary mb-2" >
                <div class="card-header text-white bg-secondary">
                    Datos de entrega
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="fecha">Tipo entrega</label>
                            <input type="text" class="form-control" value="{{ $pedido->getTipoEntrega() }}" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fecha">Entrega</label>
                            <input type="text" class="form-control" value="{{ $pedido->entrega->format("Y-m-d h:i a") }}" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="fecha">Entregar a</label>
                            <input type="text" class="form-control" value="{{ $pedido->despacho->destinatario }}" readonly>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="telefono">Teléfono</label>
                            <input type="text" class="form-control" value="{{ $pedido->despacho->telefono }}" readonly>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="correo">Correo</label>
                            <input type="text" class="form-control" value="{{ $pedido->despacho->correo }}" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="fecha">Direccion</label>
                            <input type="text" class="form-control" value="{{ $pedido->despacho->direccion }}" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fecha">Referencia</label>
                            <input type="text" class="form-control" value="{{ $pedido->despacho->referencia }}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if($pedido->total>0 && $pedido->detallePedido->count()>0 && $pedido->estado==\App\Models\Pedido::ESTADO_SIN_CONFIRMAR)
                        <form action="" method="post">
                            @csrf
                            <button type="submit" class="btn btn-success float-right">Confirmar pedido</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
