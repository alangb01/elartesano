@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <h4 class="float-left">Seleccionar productos</h4>
            <a href="{{ route('intranet.pedidos.listado') }}" class="float-right btn btn-secondary mb-2">Volver</a>
            <div class="clearfix">

            </div>
            <form method="post">
                @csrf

                <div class="form-group">
                    <label for="cliente">Cliente</label>
                    <input type="text" class="form-control" id="cliente" name="cliente" aria-describedby="clienteHelp" placeholder="Ingresa un nombre/razon social" value="{{ old('cliente',$pedido->cliente) }}">
                </div>
            </form>

            <table class="table table-bordered">
                <thead class="thead-light">
                <tr>
                    {{--                <th scope="col">#</th>--}}
                    <th scope="col">Producto</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Subtotal</th>
                </tr>
                </thead>
                <tbody>
                @isset($detallePedido)
                    @forelse($detallePedido as $detalle)
                        <tr>
                            {{--                        <th></th>--}}
                            <td>{{$detalle->producto}}</td>
                            <td>{{$detalle->cantidad}}</td>
                            <td>{{$detalle->precio}}</td>
                            <td>{{$detalle->subtotal}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5"> No hay resultados</td>
                        </tr>
                    @endforelse
                    <tr>
                        <th colspan="3" class="text-right">Total</th>
                        <th>{{ $pedido->total }}</th>
                    </tr>
                @else
                    <tr>
                        <td  colspan="5"> No se han definido datos</td>
                    </tr>
                @endisset
                </tbody>
            </table>
        </div>
    </div>
@endsection
