@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <h4 class="float-left">Registrar entrega - {{ $pedido->getCodigoNumerado() }}</h4>
            <a href="{{ route('intranet.pedidos.listado') }}" class="float-right btn btn-secondary mb-2">Volver al listado de pedidos</a>
            <div class="clearfix"></div>
            <form method="post" class="form">
                @csrf
                @if($pedido->id>0)
                    @method('PUT')
                @endif
                <div class="form-row">


                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="destinatario">Entregar a</label>
                        <input type="text" class="form-control" id="destinatario" name="destinatario" aria-describedby="clienteHelp" placeholder="Ingresa un nombre/razon social" value="{{ old('cliente',$despacho->destinatario) }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese un telefono" value="{{ old('telefono',$despacho->telefono) }}">
                    </div>
                </div>

                @if($pedido->tipo_entrega=="D")
                    @include('intranet.pedidos.entrega-mapa')
                @else
                    <div class="form-row mb-2">
                        <input type="text" class="form-control" value="La persona indicada es la que recojera el producto" readonly>
                    </div>
                @endif
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>

            <div class="row">
                <div class="col-10">
                    <div class="progress my-2" style="height: 4em">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 1: Seleccionar productos </div>
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 2: Datos de contacto </div>
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 3: Programar entrega</div>
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 3: Ubicacion de entrega</div>
                    </div>
                </div>

                <div class="col-2">
                    @if($pedido->total>0 && $pedido->detallePedido->count()>0)
                        <a href="{{ $pedido->url_resumen }}" class="btn btn-warning float-right">Continuar</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

