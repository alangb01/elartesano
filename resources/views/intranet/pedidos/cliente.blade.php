@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <h4 class="float-left">Actualizar datos de cliente  - {{ $pedido->getCodigoNumerado() }}</h4>
            <a href="{{ route('intranet.pedidos.listado') }}" class="float-right btn btn-secondary mb-2">Volver al listado de pedidos</a>
            <div class="clearfix"></div>
            <form method="post" class="form">
                @csrf
                @if($pedido->id>0)
                    @method('PUT')
                @endif
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="clienteHelp" placeholder="Ingresa un nombre" value="{{ old('nombre',$cliente->nombres) }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" id="apellido" name="apellido" aria-describedby="clienteHelp" placeholder="Ingresa un apellido" value="{{ old('apellido',$cliente->apellidos) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese un telefono" value="{{ old('telefono',$cliente->telefono) }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="correo">Correo</label>
                        <input type="email" class="form-control" id="correo" name="correo" aria-describedby="emailHelp" placeholder="Ingrese un correo" value="{{ old('correo',$cliente->correo) }}">
                        {{--                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                    </div>
                </div>
{{--                <div class="form-row">--}}
{{--                    <div class="form-group col-md-6">--}}
{{--                        <label for="inputAddress">Dirección</label>--}}
{{--                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese una dirección" value="{{old('direccion',$cliente->direccion)}}">--}}
{{--                    </div>--}}
{{--                    <div class="form-group col-md-6">--}}
{{--                        <label for="inputAddress">Referencia</label>--}}
{{--                        <input type="text" class="form-control" id="referencia" name="referencia" placeholder="Ingrese una referencia" value="{{old('referencia',$cliente->referencia)}}">--}}
{{--                    </div>--}}
{{--                </div>--}}
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
            <div class="row">
                <div class="col-10">
                    <div class="progress" style="height: 3em">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 1: Seleccionar productos </div>
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 2: Datos de contacto </div>
                    </div>
                </div>

                <div class="col-2">
                    @if($pedido->total>0 && $pedido->detallePedido->count()>0)
                        <a href="{{ $pedido->url_programar }}" class="btn btn-warning float-right">Continuar</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
