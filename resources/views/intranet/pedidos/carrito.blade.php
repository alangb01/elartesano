@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <h4 class="float-left">Seleccionar productos - {{ $pedido->getCodigoNumerado() }}</h4>
            <a href="{{ route('intranet.pedidos.listado') }}" class="float-right btn btn-secondary mb-2">Volver</a>
            <div class="clearfix"></div>
            <form method="post">
                @csrf
{{--               <div class="row">--}}
{{--                   <div class="form-group col-2">--}}
{{--                       <label for="cliente">Nº pedido</label>--}}
{{--                       <input type="text" class="form-control" id="cliente" name="" aria-describedby="clienteHelp" placeholder="Ingresa un nombre/razon social" value="{{ sprintf("%'.04d\n", $pedido->id) }}">--}}
{{--                   </div>--}}
{{--                   <div class="form-group col-10">--}}
{{--                       <label for="cliente">Cliente</label>--}}
{{--                       <input type="text" class="form-control" id="cliente" name="cliente" aria-describedby="clienteHelp" placeholder="Ingresa un nombre/razon social" value="{{ old('cliente',$pedido->cliente) }}">--}}
{{--                   </div>--}}
{{--               </div>--}}

                <div class="form-group form-inline">
                    <label for="cliente">Agregar producto </label>
                    <select name="tarifa" id="" class="form-control">
                        @foreach($tarifas as $tarifa)
                            <option value="{{ $tarifa->id }}">{{ $tarifa->getDescripcion()."  =>  S/.".$tarifa->precio_unitario }}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-success">Añadir</button>
                </div>


            </form>

            <table class="table table-bordered table-sm">
                <thead class="thead-light">
                <tr>
                    {{--                <th scope="col">#</th>--}}
                    <th scope="col">Producto</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Subtotal</th>
                    <th scope="col">Opciones</th>
                </tr>
                </thead>
                <tbody>
                @isset($detallePedido)
                    @forelse($detallePedido as $detalle)
                        <tr>
                            {{--                        <th></th>--}}
                            <td>{{$detalle->producto}}</td>
                            <td>
                                <form action="{{ $detalle->url_actualizar }}" method="post"  class="form-inline">
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="id_detalle" value="{{$detalle->id}}">
                                    <button type="submit" name="accion" class="btn btn-danger btn-sm d-none" value="0"> - </button>
                                    <button type="submit" name="accion" class="btn btn-danger btn-sm " value="-1"> - </button>
                                    <input type="text" name="cantidad" class="form-control " value="{{ $detalle->cantidad }}">
                                    <button type="submit" name="accion" class="btn btn-success btn-sm " value="1"> + </button>
                                </form>
                                </td>
                            <td>{{$detalle->precio}}</td>
                            <td>{{$detalle->subtotal}}</td>
                            <td>
                                <form action="{{ $detalle->url_eliminar }}" method="post" >
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id_detalle" value="{{ $detalle->id }}">
                                    <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                                </form>

                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5"> No hay resultados</td>
                        </tr>
                    @endforelse
                    <tr>
                        <th colspan="3" class="text-right">Total</th>
                        <th>{{ $pedido->total }}</th>
                    </tr>
                @else
                    <tr>
                        <td  colspan="5"> No se han definido datos</td>
                    </tr>
                @endisset
                </tbody>
            </table>

           <div class="row">
               <div class="col-10">
                   <div class="progress" style="height: 3em">
                       <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 1: Seleccionar productos </div>
                   </div>
               </div>

               <div class="col-2">
                   @if($pedido->total>0 && $pedido->detallePedido->count()>0)
                       <a href="{{ $pedido->url_cliente }}" class="btn btn-warning float-right">Continuar</a>
                   @endif
               </div>
           </div>
        </div>
    </div>
@endsection
