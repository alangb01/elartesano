<div class="form-row">
    <div class="input-group mb-3">

        @if(isset($direcciones) && $direcciones->count()>0)
            <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Cargar direcciones registradas
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    @foreach($direcciones as $direccion)
                        <a class="dropdown-item direccion" href="#"
                           data-id="{{$direccion->id}}"
                           data-latitud="{{ $direccion->latitud }}"
                           data-longitud="{{ $direccion->longitud }}"
                           data-zoom="{{ $direccion->zoom }}"
                           data-direccion="{{ $direccion->direccion }}"
                           data-referencia="{{ $direccion->referencia }}">
                            {{ $direccion->direccion }}
                        </a>
                    @endforeach
                </div>
            </div>
        @endif

        <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingrese la dirección de entrega" value="{{old('direccion',$despacho->direccion)}}">
        <div class="input-group-append">
            {{--                            <span class="input-group-text" id="basic-addon2">@example.com</span>--}}
            <button class="input-group-text" type="button" id="btn_detectar_marcador_segun_direccion">Buscar marcador en mapa</button>
        </div>
    </div>
    <textarea name="referencia" id="referencia" class="form-control" placeholder="Ingrese una referencia para la entrega">{{ old('referencia',$despacho->referencia) }}</textarea>

</div>
<div class="form-row form-inline my-2 text-center">

    <div class="col-3">
        Latitud:
        <input type="text" name="latitud" id="latitud" class="form-control "  value="{{ old('latitud',$despacho->latitud) }}" readonly>
    </div>
    <div class="col-3">
        Longitud:
        <input type="text" name="longitud" id="longitud" class="form-control" value="{{ old('longitud',$despacho->longitud) }}" readonly>
    </div>
    <div class="col-3">
        Zoom:
        <input type="text" name="zoom" id="zoom" class="form-control" value="{{ old('zoom',$despacho->zoom) }}" readonly>
    </div>
    <div class="col-3">
        <button class="input-group-text" type="button" id="btn_detectar_direccion_segun_marcador">Cargar dirección de marcador</button>
    </div>
</div>
<input type="text" name="id_direccion" id="id_direccion" class="form-control">
<div id="mapa"></div>

@push('styles')
    <style type="text/css">
        #mapa {
            height: 500px;
        }
    </style>
@endpush
@push('scripts_footer')
    <script defer>
        var map;
        var geocoder;
        var infowindow;
        var marker;

        function initMap() {
            geocoder = new google.maps.Geocoder;
            infowindow = new google.maps.InfoWindow;

            map = new google.maps.Map(document.getElementById('mapa'), {
                center: {lat: -6.77, lng: -79.84},
                zoom: 13,
            });


            cargarMarcadorInicial();

            google.maps.event.addListener(map,'click', function(event) {
                latitud=event.latLng.lat();
                longitud=event.latLng.lng();
                zoom = map.getZoom();
                // console.log(latitud);
                asignarMarcador(latitud,longitud);
                detectarDireccionDesdeCoordenadas(latitud,longitud);
                cargarCoordenadas(latitud,longitud,zoom);
            });

            google.maps.event.addListener(map, 'zoom_changed', function() {
                zoom = map.getZoom();
                $("input#zoom").val(zoom);
            });
        }

        function cargarCoordenadas(latitud,longitud,zoom){
            $("input#latitud").val(latitud);
            $("input#longitud").val(longitud);
            $("input#zoom").val(zoom);
        }
        //
        function visualizarMapa(latitud,longitud,zoom){
            latlng={lat:latitud,lng:longitud};
            map.setCenter(latlng);
            map.setZoom(zoom);
        }
        //
        function asignarMarcador(latitud,longitud){
            latlng={lat:latitud,lng:longitud};

            if (marker == undefined){
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    animation: google.maps.Animation.DROP,
                });
            }
            else{
                marker.setPosition(latlng);
            }

            // return marker;
        }
        //
        function detectarDireccionDesdeCoordenadas(latitud,longitud) {
            latlng={lat:latitud,lng:longitud};
            geocoder.geocode({'location': latlng}, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        $("input#direccion").val(results[0].formatted_address);
                    } else {
                        window.alert('No se pudo detectar la dirección');
                    }
                } else {
                    window.alert('No se obtubo respuesta del servidor, intente nuevamente.');
                    console.log(status);
                }
            });
        }
        //
        function detectarCoordenadasDesdeDireccion(address) {
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == 'OK') {
                    latlng=results[0].geometry.location;

                    latitud=latlng.lat();
                    longitud=latlng.lng();
                    zoom=17;

                    asignarMarcador(latitud,longitud);
                    visualizarMapa(latitud,longitud,zoom);
                    cargarCoordenadas(latitud,longitud,zoom);
                } else {
                    alert('No se pudo cargar el marcador.');
                    console.log(status);
                }
            });
        }
        //
        function cargarMarcadorInicial(){
            latitud=$("input#latitud").val();
            longitud=$("input#longitud").val();
            zoom=$("input#zoom").val();

            if(latitud==="" || longitud==="" || zoom===""){
                return;
            }

            latitud=parseFloat(latitud);
            longitud=parseFloat(longitud);
            zoom=parseFloat(zoom);

            asignarMarcador(latitud,longitud);
            visualizarMapa(latitud,longitud,zoom);
        }
        //
        $(document).on('click','button#btn_detectar_marcador_segun_direccion',function(e){
            e.preventDefault();
            detectarCoordenadasDesdeDireccion($("input#direccion").val());
        });
        //
        $(document).on('click','button#btn_detectar_direccion_segun_marcador',function(e){
            e.preventDefault();

            latitud=$("input#latitud").val();
            longitud=$("input#longitud").val();
            zoom=$("input#zoom").val();

            if(latitud==="" || longitud==="" || zoom===""){
                return;
            }

            latitud=parseFloat(latitud);
            longitud=parseFloat(longitud);
            zoom=parseFloat(zoom);

            detectarDireccionDesdeCoordenadas(latitud,longitud);
            visualizarMapa(latitud,longitud,zoom);
        });
        //
        $(document).on('click','a.direccion',function(e){
            e.preventDefault();
            latitud=$(this).data('latitud');
            longitud=$(this).data('longitud');
            zoom=$(this).data('zoom');

            latitud=parseFloat(latitud);
            longitud=parseFloat(longitud);
            zoom=parseFloat(zoom);

            asignarMarcador(latitud,longitud,zoom);
            visualizarMapa(latitud,longitud,zoom);
            cargarCoordenadas(latitud,longitud,zoom);

            $("input#direccion").val($(this).data('direccion'));
            $("textarea#referencia").val($(this).data('referencia'));
            $("input#id_direccion").val($(this).data('id'));
        });


    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&callback=initMap"
            async defer></script>
@endpush
