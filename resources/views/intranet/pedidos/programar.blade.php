@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <h4 class="float-left">Programar entrega - {{ $pedido->getCodigoNumerado() }}</h4>
            <a href="{{ route('intranet.pedidos.listado') }}" class="float-right btn btn-secondary mb-2">Volver al listado de pedidos</a>
            <div class="clearfix"></div>
            <form method="post" class="form">
                @csrf
                @if($pedido->id>0)
                    @method('PUT')
                @endif
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="fecha">Fecha</label>
                        <select name="fecha" id="" class="form-control">
                            @foreach($select_fecha as $item)
                                <option value="{{ $item->id }}" {{$item->selected}}>{{ $item->valor }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="hora">Hora</label>
                        <select name="hora" id="" class="form-control">
                            @foreach($select_hora as $item)
                                <option value="{{ $item->id }}" {{$item->selected}}>{{ $item->valor }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="tipo_entrega">Tipo entrega</label>
                        <select name="tipo_entrega" id="tipo_entrega" class="form-control">
                            @foreach($select_entrega as $item)
                                <option value="{{ $item->id }}" {{$item->selected}}>{{ $item->valor }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
            <div class="row">
                <div class="col-10">
                    <div class="progress" style="height: 3em">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 1: Seleccionar productos </div>
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 2: Datos de contacto </div>
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Paso 3: Programar entrega</div>
                    </div>
                </div>

                <div class="col-2">
                    @if($pedido->total>0 && $pedido->detallePedido->count()>0)
                        <a href="{{ $pedido->url_entrega }}" class="btn btn-warning float-right">Continuar</a>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection
