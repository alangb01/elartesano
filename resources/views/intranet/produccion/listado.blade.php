@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <form class="form-inline float-left">
                <div class="form-group mb-2">
                    <h4>Listado de pedidos</h4>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="filtrar" class="sr-only">Filtrar</label>
                    <input type="text" class="form-control" id="filtrar" name="filtrar" placeholder="Ingrese una palabra" value="{{ old('filtrar') }}">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Filtrar</button>
            </form>
            {{--            <a href="{{ route('intranet.pedidos.crear') }}" class="float-right btn btn-success mb-2">Registrar</a>--}}
        </div>
        <div class="clearfix"></div>
        @isset($pedidos)
            @if($pedidos->count())
                <div class="card-columns">

                    @foreach($pedidos as $pedido)
                        @php
if($pedido->estado_produccion==\App\Models\Pedido::ESTADO_PRODUCCION_LISTO){
    $clase_container="border-success ";
    $clase_header="bg-success text-white";
}else{
    $clase_container="border-primary";
    $clase_header="bg-primary text-white";
}

                        @endphp
                        <div class="card  {{ $clase_container }} mb-3" >
                            <div class="card-header  {{ $clase_header }} "> Pedido {{ $pedido->getCodigoNumerado() }} </div>
{{--                            <div class="card-body text-primary">--}}
                            <table class="table table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($pedido->detallePedido as $detalle)
                                            <tr>
                                                <td>{{ $detalle->producto }}</td>
                                                <td class="text-center">{{ $detalle->cantidad }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

{{--                            </div>--}}
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-6">
                                    @if($pedido->estado_produccion==\App\Models\Pedido::ESTADO_PRODUCCION_PENDIENTE)
                                        <form action="{{ $pedido->url_estado }}" method="post" class="form-inline ">
                                            @csrf
                                            <input type="hidden" name="id_pedido" value="{{ $pedido->id }}">
                                            <button type="submit" class="btn btn-success btn-sm">Marcar como listo</button>
                                        </form>
                                    @endif

                                    </div>
                                    <div class="col-6">
                                        Entrega: {{ $pedido->entrega->format("h:i a") }}
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            @else
                No hay pedidos confirmados
            @endif
        @else
            No se han detectado datos
        @endif


    </div>
@endsection
