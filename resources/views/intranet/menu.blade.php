<li class="nav-item">
    <a class="nav-link" href="{{ route('intranet.productos.listado') }}">{{ __('Productos') }}</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('intranet.tarifas.listado') }}">{{ __('Tarifas') }}</a>
</li>


<li class="nav-item">
    <a class="nav-link" href="{{ route('intranet.clientes.listado') }}">{{ __('Clientes') }}</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('intranet.pedidos.listado') }}">{{ __('Pedidos') }}</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('intranet.produccion.listado') }}">{{ __('Producción') }}</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('intranet.entregas.listado') }}">{{ __('Entregas') }}</a>
</li>

<li class="nav-item">
    <a class="nav-link" href="">{{ __('Usuarios') }}</a>
</li>
