@extends('layouts.intranet')
@section('menu')
    @include('intranet.menu')
@endsection
@section('content')
    <div class="container">
        <div class="">
            <form class="form-inline float-left">
                <div class="form-group mb-2">
                    <h4>Listado de clientes</h4>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="filtrar" class="sr-only">Filtrar</label>
                    <input type="text" class="form-control" id="filtrar" name="filtrar" placeholder="Ingrese una palabra" value="{{ old('filtrar') }}">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Filtrar</button>
            </form>
            <a href="{{ route('intranet.clientes.crear') }}" class="float-right btn btn-success mb-2">Registrar cliente</a>
        </div>
        <table class="table table-bordered table-sm">
            <thead class="thead-light">
            <tr>
{{--                <th scope="col">#</th>--}}
                <th scope="col">Alias</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Correo</th>
                <th scope="col">Telefono</th>
                <th scope="col">Opciones</th>
            </tr>
            </thead>
            <tbody>
            @isset($clientes)
                @forelse($clientes as $cliente)
                    <tr>
{{--                        <th></th>--}}
                        <td>{{$cliente->alias}}</td>
                        <td>{{$cliente->nombres}}</td>
                        <td>{{$cliente->apellidos}}</td>
                        <td>{{$cliente->correo}}</td>
                        <td>{{$cliente->telefono}}</td>
                        <td>
                            <a href="{{ $cliente->url_editar }}" class="btn btn-danger btn-sm">Editar</a>
                            <form class="form-inline d-inline" method="post" action="{{$cliente->url_pedido}}">
                                @csrf
                                <input type="hidden" name="id_cliente" value="{{$cliente->id}}">
                                <button class="btn btn-success btn-sm">Registrar pedido</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5"> No hay resultados</td>
                    </tr>
                @endforelse
            @else
                <tr>
                    <td  colspan="5"> No se han definido datos</td>
                </tr>
            @endisset

            </tbody>
            @if($clientes->hasPages())
                <tr>
                    <td  colspan="5"> {{ $clientes->links() }}</td>
                </tr>
            @endif
        </table>
    </div>
@endsection
