@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ $sorteo->titulo }}
                    </div>
                    <div class="card-body">
                        {{ $sorteo->descripcion }}

                        {{ $sorteo->programado->format("Y-m-d h:i a") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
